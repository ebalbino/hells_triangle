require "test_helper"

class HellsTriangleTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::HellsTriangle::VERSION
  end

  def setup
    @triangle = [[6],[3,5],[9,7,1],[4,6,8,4]]
    @random_triangle = HellsTriangle.generate_triangle(4)
  end

  def test_trianglur_num
    assert_equal 1, HellsTriangle.triangular_num(1)
    assert_equal 3, HellsTriangle.triangular_num(2)
    assert_equal 6, HellsTriangle.triangular_num(3)
    assert_equal 10, HellsTriangle.triangular_num(4)
    assert_equal 15, HellsTriangle.triangular_num(5)
  end

  def test_if_triangle_is_valid
    assert HellsTriangle.valid_triangle?(@triangle)
  end

  def test_if_random_triangle_is_valid
    assert HellsTriangle.valid_triangle?(@random_triangle)
  end

  def test_rand_array_length
    assert_equal HellsTriangle.rand_array(4).length, 4
    assert_equal HellsTriangle.rand_array(5).length, 5
  end

  def test_sum_triangle
    assert_equal 26, HellsTriangle.sum_triangle(0, @triangle)
  end

end
