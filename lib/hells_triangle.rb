require "hells_triangle/version"

module HellsTriangle
  def self.triangular_num(num)
    if num == 0
      return 0
    end
    num + triangular_num(num - 1)
  end

  def self.valid_triangle?(triangle)
    return triangular_num(triangle.length) == triangle.inject(0) { |acc, el| acc + el.length }
  end

  # Uses set to guarantee that each row has unique numbers
  def self.rand_array(x)
    randoms = Set.new
    loop do
      randoms << Random.rand(1..9)
      return randoms.to_a if randoms.size >= x
    end
  end

  def self.generate_triangle(n)
    triangle = []
    (1..n).each { |x| triangle << rand_array(x) }
    triangle
    # x.times.map { triangle << rand_array}
  end

  def self.sum_triangle(index, triangle)
    row = triangle.shift

    if row.nil?
      return 0
    end

    if row.length == 1
      return row[index] + sum_triangle(index, triangle)
    end

    if index == row.length - 1
      if row[index] > row[index - 1]
        return row[index] + sum_triangle(index, triangle)
      else
        return row[index - 1] + sum_triangle(index - 1, triangle)
      end
    else
      if row[index] > row[index + 1]
        return row[index] + sum_triangle(index, triangle)
      else
        return row[index + 1] + sum_triangle(index + 1, triangle)
      end
    end
  end
end
