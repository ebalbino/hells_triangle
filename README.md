# HellsTriangle

Given a triangle of numbers, find the maximum total from top to bottom

## Getting started

```bash
$ git clone https://bitbucket.org/ebalbino/hells_triangle
$ bundle install
```

## Usage

Run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment with the functions provided by the module HellsTriangle.

```bash
$ bin/hells_triangle "[[1], [2,3], [3,4,5], [6,7,8,9]]"
```

`bin/hells_triangle` is a script that accepts a multidimensional array as its only argument with the output being the maximum sum of the triangle. Make sure to wrap the array in quotes or it won't be processed correctly. 


## Observed Bugs

In cases where the algorithm has to pick between two numbers that are the same, it'll always pick the second one. This leads to cases where the sum could've been a bigger number if it had chosen the first number. But since there's no way to predict what comes in the next row, there's no way to solve this in one pass. The only solution is to go through the triangle twice, choose the first instead of the second and compare the two sums. 
